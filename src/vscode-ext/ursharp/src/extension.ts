
import * as vscode from 'vscode';
export function activate(context: vscode.ExtensionContext) {
    console.log("vscode-ursharp is now active.");
}
export function deactivate() {
    console.log("vscode-ursharp is now inactive.");
}