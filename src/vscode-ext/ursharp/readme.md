# UR# - UniveralRobotics Script Language for VSCode

![image](https://user-images.githubusercontent.com/13326808/46264788-4498dd80-c529-11e8-94be-8d477b24f69c.png)

UR# (pronounced "ur [ʊər] sharp") is a simple, modern, functional programming language.
