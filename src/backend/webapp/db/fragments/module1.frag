/* includes core modules */
@include<'svd', 'position', 'ur5'>() as Fragment;

# move motors to B1S8
[-0.8161700000, -0.9686300000, -1.5230800000, -0.6746200000, 1.5930700000, -0.0211900000] 
        when speed 1.9 when acceleration 1.7 as Linear

SendSignalTool(0); 

# move motors to B4S3
[-0.8071100000, -1.9026800000, -1.1488800000, -0.0492200000, 1.5931400000, 0.0001700000]
        when speed 0.5 when acceleration 0.2 as Circular

SendSignalTool(255); # open tool