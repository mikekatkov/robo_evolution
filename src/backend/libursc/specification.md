# Introduction
  
UR# (pronounced "ur [ʊər] sharp") is a simple, modern, functional programming language.   
UR# has its roots in the C family of languages and will be immediately familiar to URScript, F# programmers.  
UR# is standardized by ECMA International as the ECMA-8922 standard and by ISO/IEC as the ISO/IEC 94220 standard.   
Rijndael UR# compiler for the UniversalRobotics is a conforming implementation of both of these standards.  

## Hello world

The "Hello, World" program is traditionally used to introduce a programming language.   
Here it is in UR#:
```FSharp
@regular
@without(Polyscope)
@use(next)
@export(off)

@include<'ur-core'>() as Reference;

popup("Hello World");
```

UR# source files typically have the file extension .frag.  
Assuming that the "Hello, World" program is stored in the file hello.frag, 
the program can be compiled with the Rijndael UR# compiler using the command line 

```powershell
ursc hello.frag
```
  
## Program structure  
The key organizational concepts in UR# are fragments, aliases, types, members, and modules. 
UR# programs consist of one or more source files.

When UR# programs are compiled, they are physically packaged into assemblies.  
Scripts typically have the file extension .ufm or .uem, depending on whether they implement ur fragment module or ur executable module. 

## Types, Expressions and Attributes  


Attrubute `@regular` has marks source code as translatable.   
**awaiting confirmation of propolar**   
Syntax: 
```FSharp
@regular
```

Attrubute `@without` is a preprocessor command for
the interpreter that is responsible for bypassing compatibility with the Polyscope panel.   
**awaiting confirmation of propolar**   
#### Syntax: 
```FSharp
@without(Polyscope)
```   

Attrubute `@use` is a preprocessor command for using later version language.  
**awaiting confirmation of propolar**   
#### Syntax: 
```FSharp
@use(next)
```

Attrubute `@export` is a preprocessor command for deny importing this module.   
**awaiting confirmation of propolar** 
#### Syntax: 
```FSharp
@export(off) or @export(deny) or @export(false)
```

Attrubute `@include` is a preprocessor command for including named-external module\library.     
As well as support multu loading modules (`@include<'module1', 'module2', 'module3'> as Fragment;`)   
*See examples include*  
#### Syntax: 
```FSharp
@include<'ur-core'>() as Reference;
```


Function indicating which coordinates to change the robot's engines.   
Unit of measure: radians    
As well as support the indication of  
speed (`when speed 1.0`),   
acceleration (`when acceleration 1.0)` and displacement type of engines (`as Linear`, `as Circular`, etc)     
*See examples move-type-functor*    
#### Syntax: 
```FSharp
[-1.42, 0.32, 0.43, 0.0, 0.546666, -0.25555E-14] 
        when speed 1.2
        when acceleration 0.9
        as Linear
```


# Lexical Structure

### Symbols 
```antlr
symbol
    : symbol_open_rbracket    '('
    : symbol_close_rbracket   ')'
    : symbol_open_sbracket    '['
    : symbol_close_sbracket   ']'
    : symbol_comma            ','
    : symbol_dot              '.'
    : symbol_bakka            '@'
    : symbol_guillemet_open   '<'
    : symbol_guillemet_close  '>'
    : symbol_end              ';'
```


### Keywords

```antlr
keyword
    : 'void'  | 'gate'          | 'vector'     | 'point'     | 'position'
    | 'speed' | 'acceleration'  | 'rotation'   | 'when'      | 'include'
    | 'class' | 'const'         | 'continue'   | 'decimal'   | 'default'
    | 'as'    | 'do'
    ;
```


### Token Identifier

```antlr
token
    : u_identifier
    | leter_symbol letter_or_digit
    | leter_symbol symbol '_'
    ! keyword
    ;
```

### Call Statament Token  
```antlr
token
    : call_statament
    | symbol_open_rbracket u_identifier? symbol_comma? symbol_close_rbracket
    | symbol_open_rbracket u_decimal? symbol_comma? symbol_close_rbracket
    ;
```

### Token function  

```antlr
token
    : function
    | u_identifier call_statament?
    ;
```

### Token decimal array harded  

```antlr
token
    : decimal_array_hard
    | decimal symbol_comma decimal symbol_comma  decimal symbol_comma  decimal symbol_comma  decimal symbol_comma  decimal symbol_comma
    ;
```

### Token when_speed and when_acc   

```antlr
token
    : when_speed
    | keyword_when keyword_speed decimal
    ;
```
```antlr
token
    : when_speed
    | keyword_when keyword_acceleration decimal
    ;
``` 

### Token as_type 

```antlr
token
    : as_type
    | keyword_as keyword
    ;
```

### Token move_functor  

```antlr
token
    : move_functor
    | symbol_open_sbracket decimal_array_hard symbol_close_sbracket when_speed? when_acc? as_type?
    ;
```

### Attribute preproc Token   

```antlr
token
    : move_functor
    | symbol_bakka keyword_include symbol_guillemet_open symbol_q u_identifier symbol_q symbol_guillemet_close as_type symbol_end
    ;
```
