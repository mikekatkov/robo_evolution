﻿namespace urscript.interactive
//RuntimeException.cs Функционал:содержит в себе метод RuntimeException предназначеный для выявления ошибок программирования.+
{
    using System;
    public class RuntimeException : Exception
    {
        public RuntimeException(string message) : base(message)
        {
        }
    }
}