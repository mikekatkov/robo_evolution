﻿namespace urscript.interactive
//InfusedModuleRecursionException.cs Функционал:Использует библиотеку Sys,файл выполняется при наличии рекурсивного импорта в програмном модуле,содержит метод InfusedModuleRecursionException.
{
    using System;
    public class InfusedModuleRecursionException : Exception
    {
        private readonly string _module1;

        public InfusedModuleRecursionException(string module1) => _module1 = module1;

        public override string Message =>
            $"Module '{_module1}' has recursive import.";
    }
}