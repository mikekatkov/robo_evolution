﻿namespace urscript.interactive
{
    using System.IO;

    /// <summary>
    /// Structure for description of file
    /// </summary>
    public class SourceFile
    {
        /// <param name="path">
        /// Path to file
        /// </param>
        public SourceFile(string path) => PathToFile = path;
        /// <summary>
        /// Path to file in hard drive
        /// </summary>
        public string PathToFile { get; }
        /// <summary>
        /// Name of file, without extension
        /// </summary>
        public virtual string Name => Path.GetFileNameWithoutExtension(PathToFile);
        /// <summary>
        /// File contents
        /// </summary>
        /// <remarks>
        /// Reads file on every call
        /// </remarks>
        public virtual string Content => File.ReadAllText(PathToFile).Replace("\r", "");
    }
}