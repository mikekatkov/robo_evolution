﻿namespace urscript.interactive.extensions
{	//EtcEx.cs Функционал:Для успешной работы необходимы  библиотеки:Collections.Generic;System.Linq.Предназначен для установки расширений.
    using System.Collections.Generic;
    using System.Linq;

    public static class EtcEx
    {
        public static IEnumerable<string> Dry(this IEnumerable<string> enur) => enur.Where(x => !string.IsNullOrEmpty(x));
    }
}