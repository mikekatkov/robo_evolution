﻿namespace urscript.interactive
{
    using Sprache;

    public interface IConstructorSyntax//Интерфейс Синтаксического конструктора
    {
        IResult Metadata { get; set; }
        string Construct();
    }
}