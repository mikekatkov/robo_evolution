﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class URScriptWords
    {

        // System Types
        public const string Void = "void";//массив
        public const string Gate = "gate";//порт
        public const string Fragment = "Fragment";//фрагмент координат
        public const string Linear = "Linear";//перемещение по прямой
        public const string Joint = "Joint";//смещение 
        public const string Circular = "Circular";//круговое вращение


        // URTypes
        public const string Vector = "vector";//вектор
        public const string Point = "point";//точка
        public const string Position = "position";//позиция
        public const string Speed = "speed";//скорость 
        public const string Acceleration = "acc";//ускорение
        public const string Rotation = "rotation";//вразение

        // System words

        public const string When = "when";//системное слово когда
        public const string Include = "include";//сис слово включая
        public const string As = "as";//системное слово как
        



        #region Etc

        public static HashSet<string> ReservedWords { get; } =
            GetStrings(AllStringConstants.ToArray());
        private static HashSet<string> GetStrings(params string[] strings) =>
            new HashSet<string>(strings, StringComparer.OrdinalIgnoreCase);
        private static IEnumerable<string> AllStringConstants =>
            typeof(URScriptWords).GetTypeInfo().GetFields().Select(f => f.GetValue(null)).OfType<string>();

        #endregion
    }
}