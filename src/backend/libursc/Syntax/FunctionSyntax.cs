﻿namespace urscript.interactive
{
    using System.Collections.Generic;//библиотека Коллекции
    using System.IO;//Библиотека исключений
    using System.Linq;//Библиотека
    using Sprache;//Бибилиотека
    /// <summary>
    /// Function syntax meta-data
    /// </summary>
    public class FunctionSyntax : IConstructorSyntax
    {
        public string Identifier { get; set; }
        public IEnumerable<string> Parameters { get; set; }

        public override string ToString()//перееопределение метода вывода полей обьекта
        {
            return Parameters.Any() ? 
                $"Call [{Identifier}] when {{{string.Join("|", Parameters)}}}" :
                $"Call [{Identifier}]";
        }

        public IResult Metadata { get; set; }

        public string Construct()
        {
            // -= Is call moving tool?
            if (Identifier == "nt_move_tool" && !Parameters.Any())
                throw new RuntimeException("MoveTool: expected argument.");
            if (Identifier == "nt_move_tool")
                return ReadMoveTool(Parameters.First());
            // -=
            return Parameters.Any() ? 
                $"{Identifier}({string.Join(",", Parameters)})" : 
                $"{Identifier}()";
        }

        private string ReadMoveTool(string value)
        {
            if (!File.Exists("./modbus_base.rb"))
                throw new RuntimeException("File 'modbus_base.rb' not found.");
            return File.ReadAllText("./modbus_base.rb").Replace("\r", "").Replace("$POS_CLOSE", value);
        }
    }
}