﻿namespace ur5.core
{
    using System;
    using System.Globalization;
    using System.Linq;
    using etc;
    using network;

    public class URConvert
    {
        
        public static readonly object SyncGuarder = new object();
        public static void FromBytes(byte[] data)
        {
        }

        public delegate void a(UR5Info info);

        public static event a OnInfo;
        
        public static bool isDebug;

        public static UR5Info Info { get; set; }

        /// <summary>
        /// Get packet size according to the byte array
        /// </summary>
        public static int packet_size(IURBufferReader buff)
        {
            if (buff.Lenght < 4)
                return 0;
            return buff.readInt32();
        }
        /// <summary>
        /// Check if a packet is complete
        /// </summary>
        public static bool packet_check(IURBufferReader buff) => buff.Lenght >= packet_size(buff);
    }

    public static class Ex
    {
        public static string Round(this double a, int c)
        {
            return Math.Round(a, c).ToString("F10", CultureInfo.InvariantCulture);
        }
    }
}