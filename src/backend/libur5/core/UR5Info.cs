﻿namespace ur5.core
{
    using System.Collections.Generic;
    using etc;
    using sections;

    public class UR5Info
    {
        public long TimeStamp { get; set; }


        public bool IsReal { get; set; }
        public bool IsPhysical { get; set; }
        /// <summary>
        /// Robot power on
        /// </summary>
        public bool IsPowerOn { get; set; }
        /// <summary>
        /// Emergency button has down
        /// </summary>
        public bool IsEmergencyStopped { get; set; }
        /// <summary>
        /// Security alert
        /// </summary>
        public bool IsSecurityStopped { get; set; }
        /// <summary>
        /// Has program working
        /// </summary>
        public bool IsProgramWork { get; set; }
        /// <summary>
        /// Has program paused
        /// </summary>
        public bool IsProgramPaused { get; set; }
        /// <summary>
        /// Current Robot Mode
        /// </summary>
        public RobotMode Mode { get; set; }
        /// <summary>
        /// Maybe current speed.
        /// </summary>
        public double SpeedFaction { get; set; }

        public ToolPosition ToolPosition = new ToolPosition();
        public ToolOrientation ToolOrientation = new ToolOrientation();

        public List<JointSector> Joints = new List<JointSector>(6)
        {
            new JointSector(JointType.Base),
            new JointSector(JointType.Shoulder),
            new JointSector(JointType.Elbow),
            new JointSector(JointType.Wrist1),
            new JointSector(JointType.Wrist2),
            new JointSector(JointType.Wrist3)
        };

        public string ToStrJointPos()
        {
            return
                $"[{Joints[0].JointPosition:F7}, {Joints[1].JointPosition:F7}, {Joints[2].JointPosition:F7}, {Joints[3].JointPosition:F7}, {Joints[4].JointPosition:F7}, {Joints[5].JointPosition:F7}]";
        }
        public string ToStrJointTarget()
        {
            return
                $"[{Joints[0].JointTarger:F7}, {Joints[1].JointTarger:F7}, {Joints[2].JointTarger:F7}, {Joints[3].JointTarger:F7}, {Joints[4].JointTarger:F7}, {Joints[5].JointTarger:F7}]";
        }


    }
    public enum JointType
    {
        Base,
        Shoulder,
        Elbow,
        Wrist1,
        Wrist2,
        Wrist3
    }
}