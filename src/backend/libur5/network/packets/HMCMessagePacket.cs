﻿namespace ur5.network.packets
{
    using core;
    using models;

    public class HMCMessagePacket : BasePacket
    {
        private HMCMessage hmcMessage;
        public HMCMessagePacket(IURBufferReader stream, PacketState state) : base(stream, state)
        {
        }

        protected override void ReadImpl(IURBufferReader buffer)
        {
            var msgLen = getPacketState().CurrentMessageLength;
            hmcMessage = new HMCMessage
            {
                id = buffer.readInt32(),
                buffer = new byte[msgLen - 9]
            };
            // wtf
            for (var byteCount = 0; 
                byteCount < msgLen - 9; 
                byteCount += buffer.read(hmcMessage.buffer, byteCount, msgLen - 9 - byteCount)) { }
            
        }

        protected override void ExecuteImpl(UR5Info info)
        {
            HMC.DeliverHMCMessage(hmcMessage);
        }
    }
}