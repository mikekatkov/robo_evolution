﻿namespace ur5.network.packets
{
    using System;
    using System.Text;
    using System.Threading;
    using core;
    using etc;
    using exceptions;
    using models;

    public class RobotMessagePacket : BasePacket
    {
        private RobotMessage msg;
        public RobotMessagePacket(IURBufferReader stream, PacketState state) : base(stream, state)
        {
        }

        protected override void ReadImpl(IURBufferReader buffer)
        {
            msg = new RobotMessage
            {
                TimeStamp = buffer.readInt64(getPacketState()),
                Source = buffer.readByte(getPacketState()),
                Type = (RobotMessageType) buffer.readByte(getPacketState())
            };
            int getLen() => getPacketState().CurrentMessageLength - getPacketState().MessageOffset;
            switch (msg.Type)
            {
                case RobotMessageType.VERSION:
                    var len_version = buffer.readByte(getPacketState());
                    var data_version = new byte[len_version];
                    buffer.read(data_version, 0, len_version);
                    getPacketState().MessageOffset += len_version;
                    var signalsCount = buffer.readByte(getPacketState());
                    int minor = buffer.readByte(getPacketState());
                    var signalNameLength = buffer.readInt32(getPacketState());
                    var calibrationStatus = getPacketState().CurrentMessageLength - getPacketState().MessageOffset;
                    var build = new byte[calibrationStatus];
                    buffer.read(build, 0, calibrationStatus);

                    var NeverUsedUltraVar10000Maximal 
                        = getPacketState().MessageOffset + calibrationStatus;

                    var version = new URVersion(
                        getString(data_version), 
                            signalsCount, minor, 
                            signalNameLength, getString(build));

                    msg.Message = version.ToString();
                    URVersion.CurrentVersion = version;
                    LibLog.Trace(msg.Message);
                    break;
                case RobotMessageType.SECURITY:
                    msg.ErrorCode = buffer.readInt32(getPacketState());
                    msg.ErrorArgument = buffer.readInt32(getPacketState());
                    var security_code = getPacketState().CurrentMessageLength - getPacketState().MessageOffset;
                    var security_data = new byte[security_code];
                    buffer.read(security_data, 0, security_code);
                    msg.Message = getString(security_data);
                    if (msg.ErrorCode == 0xBE || msg.ErrorCode == 0xDEAD)
                        SendStopProgram();
                    
                    URDialog.Notifity("SECURITY_STOP", msg.Message);
                    break;
                case RobotMessageType.ROBOTCOMM:
                    msg.ErrorCode = buffer.readInt32(getPacketState());
                    msg.ErrorArgument = buffer.readInt32(getPacketState());
                    var lenRbcm = getLen();
                    var dataRbcm = new byte[lenRbcm];
                    buffer.read(dataRbcm, 0, lenRbcm);
                    msg.Message = getString(dataRbcm);
                    if (msg.Source == -4 && msg.ErrorCode == 190)
                        SendStopProgram();
                    break;
                case RobotMessageType.MESSAGE_KEY:
                    var msgKeyOpCode = buffer.readInt32(getPacketState());
                    var lenKeyMsg = buffer.readInt32(getPacketState());
                    signalsCount = buffer.readByte(getPacketState());
                    var keydata = new char[signalsCount];

                    for (signalNameLength = 0; signalNameLength < keydata.Length; ++signalNameLength)
                        keydata[signalNameLength] = (char)buffer.readByte(getPacketState());

                    var extradata = new char[getLen()];

                    for (calibrationStatus = 0; calibrationStatus < extradata.Length; ++calibrationStatus)
                        extradata[calibrationStatus] = (char)buffer.readByte(getPacketState());

                    msg.Title = new string(keydata);
                    msg.Message = new string(extradata);

                    if (msg.Title == "POWERDOWN_REQUEST")
                    {
                        /*
                           if (robotMessage.source == -3 && code != 100) {
                              doWorkRunnable = new Runnable() {
                                 public void run() {
                                    ShutdownManager.shutdown();
                                 }
                              };
                              SwingUtilities.invokeLater(doWorkRunnable);
                           } else {
                              doWorkRunnable = new Runnable() {
                                 public void run() {
                                    ControllerSocket.powerButtonPressed();
                                 }
                              };
                              SwingUtilities.invokeLater(doWorkRunnable);
                           }
                         */
                        URDialog.Notifity("POWERDOWN_REQUEST", msg.Message);
                        // TODO
                        Thread.Sleep(5000);
                        throw new PowerDownRequestException();
                    }
                    break;
                case RobotMessageType.VARIABLE:
                    var opcodeVar = buffer.readInt32(getPacketState());
                    var lenVar = buffer.readInt32(getPacketState());
                    signalsCount = buffer.readByte(getPacketState());
                    keydata = new char[signalsCount];

                    for (signalNameLength = 0; signalNameLength < keydata.Length; ++signalNameLength)
                        keydata[signalNameLength] = (char)buffer.readByte(getPacketState());

                    extradata = new char[getLen()];

                    for (calibrationStatus = 0; calibrationStatus < extradata.Length; ++calibrationStatus)
                        extradata[calibrationStatus] = (char)buffer.readByte(getPacketState());

                    msg.Title = new string(keydata);
                    msg.Message = new string(extradata);
                    break;
                case RobotMessageType.REQUEST_VALUE:
                    msg.ErrorCode = buffer.readInt32(getPacketState());
                    msg.ErrorArgument = buffer.readInt32(getPacketState());
                    if (msg.ErrorArgument == 0x8)
                    {
                        var messageHasError = buffer.readBool(getPacketState());
                        var error =  buffer.readBool(getPacketState());
                        var blocking =  buffer.readBool(getPacketState());
                        var extraData = new MessageExtraData(messageHasError, error, blocking);
                        msg.ExtraData = extraData;
                        signalNameLength = buffer.readByte(getPacketState());
                        var signalNameChars = new char[signalNameLength];

                        for (var i = 0; i < signalNameChars.Length; ++i)
                            signalNameChars[i] = (char) buffer.readByte(getPacketState());

                        msg.Title = new string(signalNameChars);
                    }

                    var dataM = new char[getLen()];

                    for (var byteCount = 0; byteCount < dataM.Length; ++byteCount)
                        dataM[byteCount] = (char) buffer.readByte(getPacketState());

                    msg.Message = new string(dataM);

                    break;
                case RobotMessageType.TEXT:
                    var tpcode = getPacketState().CurrentMessageLength - getPacketState().MessageOffset;
                    var data = new byte[tpcode];
                    buffer.read(data, 0, tpcode);
                    msg.Message = Encoding.UTF8.GetString(data); // TODO
                    break;
                case RobotMessageType.PROGRAM_LABEL:
                    var size_label = getPacketState().CurrentMessageLength - getPacketState().MessageOffset;
                    msg.ID = buffer.readInt32(getPacketState());
                    var char_data = new char[size_label];

                    for (var byteCount = 0; byteCount < char_data.Length; ++byteCount)
                        char_data[byteCount] = (char)buffer.readByte(getPacketState());

                    msg.Message = new string(char_data); // TODO
                    break;
                case RobotMessageType.POPUP:
                default:
                    while (getPacketState().MessageOffset < getPacketState().CurrentMessageLength)
                        buffer.readByte(getPacketState());
                    throw new Exception($"Unknown RobotMessage type {msg.Type}");
            }

        }

        private void SendStopProgram()
        {
            HightLevelSocket.Instance.Send("stop program\n");
            ValueRequest.Clear();
        }

        protected override void ExecuteImpl(UR5Info info)
        {
            RobotMessage.Notifity(msg);
        }
    }
}