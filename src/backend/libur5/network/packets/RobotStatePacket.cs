﻿namespace ur5.network.packets
{
    using core;
    using etc;

    public class RobotStatePacket : BasePacket
    {
        public RobotStatePacket(IURBufferReader stream, PacketState state) : base(stream, state) { }

        protected override void ReadImpl(IURBufferReader reader)
        {
            var @base = new UR5Info();
            var state = getPacketState();

            while (reader.Offset < state.CurrentMessageLength)
            {
                var packet_start = reader.getOffset();
                var box_size = reader.readInt32(state);
                var box_type = reader.readByte(state);
                switch (box_type)
                {
                    case 0:
                        @base.TimeStamp = reader.readInt64(state);
                        @base.IsPhysical = reader.readBool(state);
                        @base.IsReal = reader.readBool(state);
                        @base.IsPowerOn = reader.readBool(state);
                        @base.IsEmergencyStopped = reader.readBool(state);
                        @base.IsSecurityStopped = reader.readBool(state);
                        @base.IsProgramWork = reader.readBool(state);
                        @base.IsProgramPaused = reader.readBool(state);
                        @base.Mode = (RobotMode)reader.readByte(state);
                        @base.SpeedFaction = reader.readDouble(state); // speed fraction
                        break;
                    case 1:
                        {
                            var code = 0;
                            while (true)
                            {
                                if (code >= 6)
                                    break;
                                @base.Joints[code].JointPosition = reader.readDouble(state);
                                @base.Joints[code].JointTarger = reader.readDouble(state);
                                @base.Joints[code].JointSpeed = reader.readDouble(state);
                                @base.Joints[code].JointCurrent = reader.readFloat(state);
                                @base.Joints[code].Voltage = reader.readFloat(state);
                                @base.Joints[code].MotorTemperature = reader.readFloat(state);
                                @base.Joints[code].MicroTemperature = reader.readFloat(state);
                                @base.Joints[code].Mode = reader.readByte(state);
                                code++;
                            }
                        }
                        break;
                    case 2:
                        {
                            reader.readByte(getPacketState()); // analogInputRange[2]
                            reader.readByte(getPacketState()); // analogInputRange[3]
                            reader.readDouble(getPacketState()); // analogIn[2]
                            reader.readDouble(getPacketState()); // analogIn[3]
                            reader.readFloat(getPacketState()); // toolVoltage48V
                            reader.readByte(getPacketState()); // toolOutputVoltage
                            reader.readFloat(getPacketState()); // toolCurrent
                            reader.readFloat(getPacketState()); // toolTemperature
                            reader.readByte(getPacketState()); // toolMode
                        }
                        break;
                    case 3: // digital, robot data, euromap and etc
                        var digitalInputBits = reader.readInt16(state);
                        var digitalOutputBits = reader.readInt16(state);

                        reader.readByte(state); // analogInputRange 0
                        reader.readByte(state); // analogInputRange 1

                        reader.readDouble(state); // analogIn 0
                        reader.readDouble(state); // analogIn 1

                        reader.readByte(state); // analogOutputDomain 0
                        reader.readByte(state); // analogOutputDomain 1

                        reader.readDouble(state); // analogOut 0
                        reader.readDouble(state); // analogOut 1

                        reader.readFloat(state);  // masterTemperature
                        reader.readFloat(state);  // robotVoltage48V
                        reader.readFloat(state);  // robotCurrent
                        reader.readFloat(state);  // masterIOCurrent

                        reader.readByte(state);  // masterSafetyState
                        reader.readByte(state);  // masterOnOffState

                        var signalResponseTimeInMs = reader.readByte(state);

                        if (signalResponseTimeInMs == 1)
                        {
                            var euromapInputBits = reader.readInt32(state);
                            var euromapOutputBits = reader.readInt32(state);
                            var euromap24V_voltage = reader.readInt16(state);
                            var euromap24V_current = reader.readInt16(state);
                        }


                        break;
                    case 4:
                        {
                            @base.ToolPosition.X = (float) reader.readDouble(getPacketState());
                            @base.ToolPosition.Y = (float) reader.readDouble(getPacketState());
                            @base.ToolPosition.Z = (float) reader.readDouble(getPacketState());

                            @base.ToolOrientation.X = (float) reader.readDouble(state);
                            @base.ToolOrientation.Y = (float) reader.readDouble(getPacketState());
                            @base.ToolOrientation.Z = (float) reader.readDouble(getPacketState());
                        }
                        break;
                    case 5: // KinematicsCalibration
                        for (var i = 0; i < 6; i++)
                            reader.readInt32(getPacketState()); // jointChecksum
                        for (var i = 0; i < 6; i++)
                            reader.readDouble(getPacketState()); // deltaTheta
                        for (var i = 0; i < 6; i++)
                            reader.readDouble(getPacketState()); // KinematicsCalibration A
                        for (var i = 0; i < 6; i++)
                            reader.readDouble(getPacketState()); // KinematicsCalibration D
                        for (var i = 0; i < 6; i++)
                            reader.readDouble(getPacketState()); // KinematicsCalibration alpha
                        var calibrationStatus = reader.readInt32(getPacketState());
                        break;
                    case 6:
                        for (var i = 0; i < 6; i++)
                        {
                            // JointMinLimit
                            reader.readDouble(state);
                            // JointMaxLimit
                            reader.readDouble(state);
                        }
                        for (var i = 0; i < 6; i++)
                        {
                            // JointMaxSpeed
                            reader.readDouble(state);
                            // JointMaxAcceleration
                            reader.readDouble(state);
                        }

                        reader.readDouble(state); // VJointDefault
                        reader.readDouble(state); // AJointDefault

                        reader.readDouble(state); // VToolDefault
                        reader.readDouble(state); // AToolDefault
                        reader.readDouble(state); // EqRadius

                        for (var i = 0; i < 6; i++) reader.readDouble(state); // DHa
                        for (var i = 0; i < 6; i++) reader.readDouble(state); // DHd
                        for (var i = 0; i < 6; i++) reader.readDouble(state); // DHalpha
                        for (var i = 0; i < 6; i++) reader.readDouble(state); // DHtheta

                        MotherBoard.Version = reader.readInt32(state);
                        MotherBoard.ControllerBoxType = reader.readInt32(state);
                        MotherBoard.RobotType = reader.readInt32(state);
                        MotherBoard.RobotSubType = reader.readInt32(state);

                        for (var i = 0; i < 6; i++) reader.readInt32(state); // JointMotorType

                        break;
                    case 7:
                        for (var i = 0; i != 6; i++)
                            reader.readDouble(state); // force_mode_frame
                        reader.readDouble(state); // robot_dexterity
                        break;
                    case 8:
                        reader.readBool(state); // teachButtonPressed
                        reader.readBool(state); // teachButtonEnabled
                        break;
                    case 9:
                        reader.setOffset(reader.getOffset() + (box_size - 5)); // skipBytes
                        break;
                }
            }
            MotherBoard.updateState(@base);
        }

        protected override void ExecuteImpl(UR5Info info)
        {
        }
    }
}