﻿namespace ur5.network.packets
{
    using System;

    public static class PacketStorage
    {
        public static TPacket Instance<TPacket>(IURBufferReader reader, PacketState state) where TPacket : class
        {
            return (TPacket)Activator.CreateInstance(typeof(TPacket), reader, state);
        }
    }
}