﻿namespace ur5.network.packets
{
    using System;
    using System.IO;
    using core;
    using etc;

    public abstract class BasePacket
    {
        private readonly IURBufferReader currentStream;
        private readonly PacketState currentState;

        protected BasePacket(IURBufferReader stream, PacketState state)
        {
            currentStream = stream;
            currentState = state;
        }

        public void Read()
        {
            try
            {
                ReadImpl(currentStream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void Execute(UR5Info info)
        {
            try
            {
                ExecuteImpl(info);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected abstract void ReadImpl(IURBufferReader buffer);
        protected abstract void ExecuteImpl(UR5Info info);

        protected PacketState getPacketState() => this.currentState;


        protected string getString(byte[] array) => URStringEncoding.UREncoder.GetString(array);
    }
}