﻿namespace ur5.network
{
    public interface IURBufferReader
    {
        byte readByte(PacketState state = null);
        short readInt16(PacketState state = null);
        float readFloat(PacketState state = null);
        int readInt32(PacketState state = null);
        long readInt64(PacketState state = null);
        bool readBool(PacketState state = null);
        double readDouble(PacketState state = null);
        IURBufferReader Clone();
        int read(byte[] array, int offset, int lenght);
        void setOffset(long offset);
        long getOffset();
        long Offset { get; }
        long Lenght { get; }
    }
}