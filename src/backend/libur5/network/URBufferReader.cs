﻿namespace ur5.network
{
    using System;
    using System.Linq;

    public class URBufferReader : URBuffer, IURBufferReader
    {
        internal URBufferReader(byte[] bytes) : base(bytes)
        { }

        short IURBufferReader.readInt16(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(short)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(short));
            if(state != null) state.MessageOffset += sizeof(short);
            return BitConverter.ToInt16(@Byte.Reverse().ToArray(), startIndex: 0);
        }
        byte IURBufferReader.readByte(PacketState state = null)
        {
            Byte[] @Byte = new byte[1];
            nMStream.Read(@Byte, offset: 0, count: @Byte.Length);
            if(state != null) state.MessageOffset += sizeof(byte);
            return @Byte[0];
        }
        float IURBufferReader.readFloat(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(float)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(float));
            if(state != null) state.MessageOffset += sizeof(float);
            return BitConverter.ToSingle(@Byte.Reverse().ToArray(), startIndex: 0);
        }
        int IURBufferReader.readInt32(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(int)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(int));
            if(state != null) state.MessageOffset += sizeof(int);
            return BitConverter.ToInt32(@Byte.Reverse().ToArray(), startIndex: 0);
        }
        long IURBufferReader.readInt64(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(long)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(long));
            if(state != null) state.MessageOffset += sizeof(long);
            return BitConverter.ToInt64(@Byte.Reverse().ToArray(), startIndex: 0);
        }
        double IURBufferReader.readDouble(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(double)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(double));
            if(state != null) state.MessageOffset += sizeof(double);
            return BitConverter.ToDouble(@Byte.Reverse().ToArray(), startIndex: 0);
        }
        IURBufferReader IURBufferReader.Clone()
        {
            var reader = (URBufferReader)Create(nMStream.ToArray());
            reader.nMStream.Position = nMStream.Position;
            return reader;
        }

        int IURBufferReader.read(byte[] array, int offset, int lenght) => nMStream.Read(array, offset, lenght);
        void IURBufferReader.setOffset(long offset) => nMStream.Position = offset;

        long IURBufferReader.getOffset() => nMStream.Position;
        public long Offset => nMStream.Position;

        public long Lenght => nMStream.ToArray().Length;

        bool IURBufferReader.readBool(PacketState state = null)
        {
            byte[] @Byte = new byte[sizeof(bool)];
            nMStream.Read(@Byte, offset: 0, count: sizeof(bool));
            if(state != null) state.MessageOffset += sizeof(byte);
            return BitConverter.ToBoolean(@Byte, startIndex: 0);
        }


    }
}