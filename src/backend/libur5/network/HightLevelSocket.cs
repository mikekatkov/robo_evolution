﻿namespace ur5.network
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using core;
    using etc;
    using packets;

    public class HightLevelSocket : LowLevelSocket
    {
        public static HightLevelSocket Instance { get; }
        public static readonly object SyncGuarder = new object();
        #region Constructors
        public HightLevelSocket(EndPoint endpoint) : base(endpoint) { }
        public HightLevelSocket(EndPoint endpoint, Encoding encoding) : base(endpoint, encoding)  { }
        public HightLevelSocket(string host, int port) : base(host, port)  { }
        public HightLevelSocket(string host, int port, Encoding encoding) : base(host, port, encoding) {  }
        internal HightLevelSocket(Socket socket) : base(socket) { }
        #endregion

        
        private void InternalProcessPacket(byte[] data)
        {
            var buffer = URBuffer.Create(data);
            var state = new PacketState(size: buffer.readInt32());
            var opcode = state.CurrentMessageOpCode = buffer.readByte();
            

            TPacket At<TPacket>() where TPacket : class 
                => PacketStorage.Instance<TPacket>(buffer, state);

            switch (opcode)
            {
                case -0x1: Disconnect();                    break;
                case 0x5 : At<ModbusPacket>      ().Read(); break;
                case 0x10: At<RobotStatePacket>  ().Read(); break;
                case 0x14: At<RobotMessagePacket>().Read(); break;
                case 0x16: At<HMCMessagePacket>  ().Read(); break;
                default:
                    var msg = $"Unknown message type '{opcode:X2}' (last message type was {LastOpCode:X2})";
                    LibLog.Error(msg);
                break;
            }
            LastOpCode = opcode;
        }

        private void bla()
        {
            dynamic data = new object();
            int packet_size(dynamic a) => 1;
            var reader = URBuffer.Create(data);
                var @base = new UR5Info();

                var size = packet_size(reader);
                if (size > reader.Lenght)
                    return;

                var opcode = reader.readByte();

                if (opcode != 16)
                    return;

                while (reader.getOffset() < size)
                {
                    var packet_start = reader.getOffset();
                    var box_size = reader.readInt32();
                    var box_type = reader.readByte();
                    switch (box_type)
                    {
                        case 0:
                            @base.TimeStamp = reader.readInt64();
                            @base.IsPhysical = reader.readBool();
                            @base.IsReal = reader.readBool();
                            @base.IsPowerOn = reader.readBool();
                            @base.IsEmergencyStopped = reader.readBool();
                            @base.IsSecurityStopped = reader.readBool();
                            @base.IsProgramWork = reader.readBool();
                            @base.IsProgramPaused = reader.readBool();
                            @base.Mode = (RobotMode)reader.readByte();
                            @base.SpeedFaction = reader.readDouble(); // speed fraction

                            //isProgramWork = @base.IsProgramWork;
                            break;
                        case 1:
                            {
                                var code = 0;
                                while (true)
                                {
                                    if (code >= 6)
                                        break;
                                    @base.Joints[code].JointPosition = reader.readDouble();
                                    @base.Joints[code].JointTarger = reader.readDouble();
                                    @base.Joints[code].JointSpeed = reader.readDouble();
                                    @base.Joints[code].JointCurrent = reader.readFloat();
                                    @base.Joints[code].Voltage = reader.readFloat();
                                    @base.Joints[code].MotorTemperature = reader.readFloat();
                                    @base.Joints[code].MicroTemperature = reader.readFloat();
                                    @base.Joints[code].Mode = reader.readByte();
                                    code++;
                                }
                                // ReSharper disable once CompareOfFloatsByEqualityOperator
                                //isMove = @base.Joints.All(x => x.JointSpeed == 0);
                            }
                            break;
                        case 2:
                            {
                                reader.readByte(); // analogInputRange[2]
                                reader.readByte(); // analogInputRange[3]
                                reader.readDouble(); // analogIn[2]
                                reader.readDouble(); // analogIn[3]
                                reader.readFloat(); // toolVoltage48V
                                reader.readByte(); // toolOutputVoltage
                                reader.readFloat(); // toolCurrent
                                reader.readFloat(); // toolTemperature
                                reader.readByte(); // toolMode
                            }
                            break;
                        case 3:
                            reader.setOffset(reader.getOffset() + 50);
                            var signalResponseTimeInMs = reader.readByte();
                            if (signalResponseTimeInMs == 1)
                                reader.setOffset(reader.getOffset() + 13);
                            break;
                        case 4:
                            {
                                @base.ToolPosition.X = (float) reader.readDouble();
                                @base.ToolPosition.Y = (float) reader.readDouble();
                                @base.ToolPosition.Z = (float) reader.readDouble();

                                @base.ToolOrientation.X = (float) reader.readDouble();
                                @base.ToolOrientation.Y = (float) reader.readDouble();
                                @base.ToolOrientation.Z = (float) reader.readDouble();
                            }
                            break;
                        case 7:
                            for (var i = 0; i != 6; i++)
                                reader.readDouble();
                            reader.readDouble();
                            break;
                        case 8:
                            reader.readBool();
                            reader.readBool();
                            break;
                        case 9:
                            reader.setOffset(reader.getOffset() + (box_size - 5));
                            break;
                    }
                }
        }

        public void ProcessPacket(byte[] data)
        {
            lock (SyncGuarder)
            {
                InternalProcessPacket(data);
            }
        }



        public void FreeDrive() => Send("set robotmode freedrive\n");
        public void RunMode() => Send("set robotmode run\n");
        public void Pause() => Send("pause program\n");
        public void Resume() => Send("resume program\n");
        public void RunProgram() => Send("run program\n");
        public void StopProgram() => Send("stop program\n");


        #region Privates

        private int LastOpCode { get; set; }

        #endregion
    }
}