﻿namespace ur5.etc
{
    using System;

    public static class URDialog
    {
        public static void Notifity(string key, string message)
        {
            Console.WriteLine($"URDialog [{key}]: {message}");
        }

        public static void SecurityStoppedDialog()
        {
            Console.WriteLine($"URDialog [SecurityStopped]");
        }

        public static void BadRobotStateDialog(string toString)
        {
            Console.WriteLine($"URDialog [BadState]: {toString}");

        }
    }
}