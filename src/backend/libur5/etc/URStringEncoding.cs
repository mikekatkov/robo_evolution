﻿namespace ur5.etc
{
    using System.Text;

    public static class URStringEncoding
    {
        public static Encoding UREncoder { get; set; } = Encoding.ASCII;
    }
}