﻿namespace ur5.etc
{
    public class Position3D
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}