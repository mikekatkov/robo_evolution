﻿namespace libur5_test
{
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public abstract class CorpusTest
    {
        public static async Task TimeoutAfter(Task task, int millisecondsTimeout)
        {
            if (task == await Task.WhenAny(task, Task.Delay(millisecondsTimeout)))
                await task;
            else
                throw new TimeoutException();
        }
    }
}