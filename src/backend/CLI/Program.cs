﻿namespace CLI
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using CLIMapper;
    using RC.Framework.Screens;
    using ur5.core;
    using ur5.etc;
    using ur5.network.packets;
    using urscript.interactive;

    class Program
    {

        public static void Sync(bool start = true)
        {
            if (start && mmap.ContainsKey("ts"))
            {
                var ts = mmap["ts"].ToString();
                Screen.WriteLine($"Wait {ts}ms...".To(Color.Yellow));
                Thread.Sleep(int.Parse(ts));
                return;
            }
            if (!start && mmap.ContainsKey("tq"))
            {
                var ts = mmap["tq"].ToString();
                Screen.WriteLine($"Wait {ts}ms...".To(Color.Yellow));
                Thread.Sleep(int.Parse(ts));
                return;
            }

            if (!mmap.ContainsKey("d")) // if -d keycode for waiting masterboard
                return;

            Screen.WriteLine($"Wait signal from masterboard...".To(Color.Yellow));
            while (true)
            {
                if (MotherBoard.CurrentState == DashBoardState.STOPPED)
                    break;
                if (MotherBoard.CurrentState != DashBoardState.PAUSED)
                    continue;
                Screen.WriteLine($"Detected dead-lock. unlocking...".To(Color.Gray));
                MotherBoard.Stop();
            }
        }

        private static void Sleep(int val)
        {
            Thread.Sleep(!mmap.ContainsKey("force") ? val : 20);
        }
        private static Dictionary<string, object> mmap;
        private static Thread thread;
        static void Main(string[] args)
        {
            RCL.EnablingVirtualTerminalProcessing();
            RCL.SetThrowCustomColor(false);
            Screen.WriteLine("Command Line Interface UR5 v1.9.48");
            Screen.WriteLine("2018 (C) Yuuki Wesp\n");
            var map = Mapper.Map(args, "-");
            mmap = map;
            if (!map.ContainsKey("i") || !map.ContainsKey("p"))
            {
                PrintHelp();
                return;
            }
            var ip = (string)map["i"];
            var port = (int)map["p"];

            try
            {
                Screen.WriteLine("Connection to URController...".To(Color.Gray));
                LibLog.Skip<ModbusPacket>();
                LibLog.RegisterHandle((code, data) =>
                {
                    if(code == 0 && !map.ContainsKey("debug"))
                        return;
                    Screen.WriteLine($"[{code.ToString().To(Color.Red)}]: {data}");
                });
                if (!map.ContainsKey("emu"))
                MotherBoard.CreateSocket(ip, port);
                
            }
            catch (Exception e)
            {
                Screen.WriteLine($"Failed connect to UR5".To(Color.Red));
                Screen.WriteLine($"{e.Message}");
                return;
            }
            Screen.WriteLine("Await robot state...");

            if (map.ContainsKey("emu"))
            {
                MotherBoard.LastActualState = new UR5Info();
            }

            while (true)
            {
                Sleep(200);
                if (MotherBoard.LastActualState != null)
                    break;
            }

            StartConsoleTitle();

            if (map.ContainsKey("reset"))
            {
                MotherBoard.Stop();
                Thread.Sleep(20);
            }
            Sync(true);
            var info = MotherBoard.LastActualState;
            if (map.ContainsKey("fd"))
            {
                MotherBoard.Freedrive();
                Screen.WriteLine("Press any key for disable freedrive.".To(Color.Gray));
                Console.ReadKey();
                MotherBoard.Freedrive();
                info = MotherBoard.LastActualState;
                Screen.WriteLine($"Current pos: [{info.Joints[0].JointPosition.Round(5)}, " +
                                 $"{info.Joints[1].JointPosition.Round(5)}, " +
                                 $"{info.Joints[2].JointPosition.Round(5)}, " +
                                 $"{info.Joints[3].JointPosition.Round(5)}, " +
                                 $"{info.Joints[4].JointPosition.Round(5)}, " +
                                 $"{info.Joints[5].JointPosition.Round(5)}]");
                Sleep(400);
                Close();
                return;
            }

            if (map.ContainsKey("sf"))
            {
                while (true)
                {
                    Sleep(300);
                }
            }

            if (map.ContainsKey("w"))
            {
                Sync(true);
                var pos = map["w"].ToString();

                if (int.TryParse(pos, out var val))
                {
                    var file = File.ReadAllText("./modbus_base.rb").Replace("\r", "");
                    var b = new StringBuilder();

                    b.AppendLine("def step_forward():");

                    foreach (var s in file.Split('\n'))
                        b.AppendLine($"\t{s}");

                    b.AppendLine("end");
                    b.AppendLine("step_forward()");

                    var newStr = b.ToString();

                    newStr = newStr.Replace("$POS_CLOSE", val.ToString());
                    Success("-w", "Start moving tool...");
                    MotherBoard.SetNewProgram(new ur5.models.Program("<memory>", Guid.Empty.ToString(), new SourceFileInMemory(newStr, "<memory>")));
                    MotherBoard.Play();
                    Sleep(2000);
                }
                else
                {
                    Error("-w", $"invalid value: {pos}");
                }
                Close();
                return;
            }


            if (map.ContainsKey("e"))
            {
                Screen.WriteLine($"Current pos: [{info.Joints[0].JointPosition.Round(5)}, " +
                                 $"{info.Joints[1].JointPosition.Round(5)}, " +
                                 $"{info.Joints[2].JointPosition.Round(5)}, " +
                                 $"{info.Joints[3].JointPosition.Round(5)}, " +
                                 $"{info.Joints[4].JointPosition.Round(5)}, " +
                                 $"{info.Joints[5].JointPosition.Round(5)}]");
                Close();
                return;
            }

            var targetInstruction = "movej";
            var defaultAcceleration = "0.700";
            var defaultSpeed = "0.900";

            if (map.ContainsKey("t"))
            {
                var val = map["t"].ToString();

                if (val == "movej" || val == "movel")
                {
                    targetInstruction = val;
                    Success("-t", $"Success set new move model: '{targetInstruction}'");
                }
                else
                    Error("-t", $"Unknown target function '{val}'. Allowed: ['movej' or 'movel']");
            }

            if (map.ContainsKey("s"))
            {
                var val = map["s"].ToString();
                if (float.TryParse(val, out var newval))
                {
                    if (newval > 2.0 && !HaultSpeedInsecurity(val, targetInstruction))
                        Error("-s", "Set speed is denied.");
                    else
                    {
                        defaultSpeed = newval.ToString("##.000");
                        Success("-s", $"Set new speed: {getMeasure(targetInstruction, defaultSpeed)}");
                    }
                }
                else
                    Error("-s", $"Unknown target function '{val}'. Allowed: ['movej' or 'movel']");
            }

            if (map.ContainsKey("q"))
            {
                var val = map["q"].ToString();
                if (float.TryParse(val, out var newval))
                {
                    if (newval > 2.0 && !HaultSpeedInsecurity(val, targetInstruction))
                        Error("-q", "Set acceleration is denied.");
                    else
                    {
                        defaultAcceleration = newval.ToString("##.000");
                        Success("-s", $"Set new acceleration: {getMeasure(targetInstruction, defaultAcceleration)}");
                    }
                }
                else
                    Error("-s", $"Unknown target function '{val}'. Allowed: ['movej' or 'movel']");
            }


            if (map.ContainsKey("ef"))
            {
                if(File.Exists("coord.pos"))
                    File.Delete("coord.pos");
                File.WriteAllText("coord.pos", $"Current pos: [{info.Joints[0].JointPosition.Round(5)}, " +
                                 $"{info.Joints[1].JointPosition.Round(5)}, " +
                                 $"{info.Joints[2].JointPosition.Round(5)}, " +
                                 $"{info.Joints[3].JointPosition.Round(5)}, " +
                                 $"{info.Joints[4].JointPosition.Round(5)}, " +
                                 $"{info.Joints[5].JointPosition.Round(5)}]");
                Success("-ef", "Current position writed to 'coord.pos' file.");
                Close();
                return;
            }

            if (map.ContainsKey("x"))
            {
                var coord = map["x"];
                if (coord.ToString().StartsWith("[") && coord.ToString().EndsWith("]"))
                {
                    Success("-x", $"Play '{coord}' with speed {getMeasure(targetInstruction, defaultSpeed)} and acceleration {getMeasure(targetInstruction, defaultAcceleration)}...");
                    Sleep(3000);
                    var code = ($"{targetInstruction}({coord},{defaultAcceleration},{defaultSpeed},0,0)\n");
                    
                    MotherBoard.SetNewProgram(new ur5.models.Program("<memory>", 
                        Guid.Empty.ToString(), 
                        new SourceFileInMemory(code, "<memory>")));
                    MotherBoard.Play();
                }
                else
                    Error("-x", $"{coord} - is not valid format. Format: '[0,0,0,0,0,0]'");
                Close();
                return;
            }

            if (map.ContainsKey("raw"))
            {
                var file = map["raw"].ToString();
                var code = File.ReadAllText(file);
                MotherBoard.SetNewProgram(new ur5.models.Program("<memory>",
                        Guid.Empty.ToString(),
                        new SourceFileInMemory(code, "<memory>")));
                MotherBoard.Play();
                Thread.Sleep(4000);
                Close();
                return;
            }

            if (map.ContainsKey("frag"))
            {
                var file = map["frag"].ToString();
                Success("frag", "Indexing source fragments...");
                if (!Directory.Exists("db"))
                {
                    Error("frag", "Directory 'db' not found.");
                    Close();
                    return;
                }
                if (!Directory.Exists("db\\fragments"))
                {
                    Error("frag", "Directory 'db\\fragments' not found.");
                    Close();
                    return;
                }
                SourceManager.IndexingSourceFiles("./db");
                Success("frag", $"Detected '{SourceManager.DB.Count}' fragments..");
                var module = SourceManager.SafeInvokeModule(file);
                if (module == null)
                {
                    Error("frag", $"Module '{file}'.frag not found.");
                    Close();
                    return;
                }
                Success("frag", $"Compiling '{module.Name}' fragment..");
                var compiled = default(ur5.models.Program);
                try
                {
                    compiled = new ur5.models.Program("<memory>", Guid.NewGuid().ToString() , new SourceFileInMemory(URSc.Assembly(module), "<memory>"));
                }
                catch (Exception e)
                {
                    Error("frag", "\tCompilation error:");
                    Screen.WriteLine($"{e.Message}".To(Color.Red));
                    Close();
                    return;
                }
                Success("frag", $"Success compilation '{module.Name}' fragment.");
                Success("frag", $"Play '{module.Name}' fragment from UR5.");
                MotherBoard.SetNewProgram(compiled);
                MotherBoard.Play();
                Thread.Sleep(4000);
                Close();
                return;
            }

            if (map.ContainsKey("z"))
            {
                var file = map["z"].ToString();
                if (!File.Exists(file))
                {
                    Screen.WriteLine($"File '{file}' not found.".To(Color.Red));
                    return;
                }

                var file2 = File.ReadAllLines(file);
                var b = new StringBuilder();

                void MoveTools(string value)
                {
                    var driver = File.ReadAllText("./modbus_base.rb").Replace("\r", "").Replace("$POS_CLOSE", value);
                    
                    var sddw = driver.Split('\n').ToArray();
                    foreach (var s in sddw)
                        b.AppendLine($"\t{s}");
                }
                b.AppendLine("def step_forward():");
                
                foreach (var line in file2)
                {
                    if(string.IsNullOrEmpty(line))
                        continue;
                    if (line.First() == '#')
                    {
                        Screen.WriteLine($"[std] comment: {line}");
                        continue;
                    }
                    if (line.Contains("sleep"))
                    {
                        Screen.WriteLine($"[std] sleep_asm: {line}");
                        b.AppendLine($"\t{line}");
                        continue;
                    }

                    if (line.Contains("nt_move_tool"))
                    {
                        var line2 = line.Replace("nt_move_tool(", "").Replace(")", "");
                        Screen.WriteLine($"[std] tool_asm: {line2}");
                        MoveTools(line2);
                        continue;
                    }
                    Screen.WriteLine($"[std]: {line}");
                    b.AppendLine($"\t{targetInstruction}({line},{defaultAcceleration},{defaultSpeed},0,0)");
                }
                b.AppendLine("end");
                File.WriteAllText("output.rb", b.ToString());
                b.AppendLine("step_forward()");
                MotherBoard.SetNewProgram(new ur5.models.Program("<memory>",
                        Guid.Empty.ToString(),
                        new SourceFileInMemory(b.ToString(), "<memory>")));
                MotherBoard.Play();

            }

            Close();
        }

        private static void Close()
        {
            Sync(false);
            Sleep(1400);
            consoleThread.Abort();
            MotherBoard.ShutdownDriver();
        }

        static void PrintHelp()
        {
            Screen.WriteLine("Help:");
            Screen.WriteLine("\t--help              -   print command info");
            Screen.WriteLine("\t-p                  -   port to ur-controller");
            Screen.WriteLine("\t-i                  -   ip to ur-controller");
            Screen.WriteLine("\t-e                  -   print current coordinates to terminal");
            Screen.WriteLine("\t-ef                 -   print current coordinates to file 'coord.pos'");
            Screen.WriteLine("\t-z <file>           -   play coordinates from picked file");
            Screen.WriteLine("\t-s 'speed'          -   override speed faction [rad/s | m/s]");
            Screen.WriteLine("\t-q 'acceleration'   -   override acceleration faction [rad/s | m/s]");
            Screen.WriteLine("\t-t 'movej|movel'    -   override model instruction");
            Screen.WriteLine("\t-x '<coord>'        -   play coordinates");
            Screen.WriteLine("\t-w '255'            -   move tools");
            Screen.WriteLine("\t-ts <int>           -   wait X ms on start");
            Screen.WriteLine("\t-tq <int>           -   wait X ms on quit");
            Screen.WriteLine("\t-debug              -   enable debug output");
            Screen.WriteLine("\t-sf                 -   loop enchange");
            Screen.WriteLine("\t-d                  -   wating signal stoping motors from masterboard");
            Screen.WriteLine("\t-fd                 -   enable freedrive");
            Screen.WriteLine("\t-frag <name>        -   execute module fragment by name without extension");
        }
        /// <summary>
        /// Haha, classic
        /// </summary>
        private static bool HaultSpeedInsecurity(string speed, string type)
        {
            if (Q($"New speed has a potential danger. Do you really want to use '{getMeasure(type, speed)}' speed?".To(Color.Gray)) == "N")
                return false;
            if (Q("Really?".To(Color.Yellow)) == "N")
                return false;
            if (Q("No really, it's dangerous! Use it speed?".To(Color.Chartreuse)) == "N")
                return false;
            if (Q("Have you asked the robot permission?".To(Color.PaleVioletRed)) == "N")
                return false;
            if (Q("Have you thought it over well?".To(Color.DarkGoldenrod)) == "N")
                return false;
            var res = Q("How much will the 1+1?".To(Color.DeepSkyBlue));
            if (res != "2")
            {
                Error("-s", $"Nope. 1 + 1 != {res}");
                return false;
            }
            return true;
        }


        private static string Q(string text)
        {
            Screen.Write($"[{"Q".To(Color.Aqua)}] {text} [y/N]: ");
            var vale = Console.ReadLine();
            if (string.IsNullOrEmpty(vale))
                return "N";
            if (vale.ToUpper() == "Т")
                return "N";
            if (vale.ToUpper() == "Н")
                return "Y";
            return vale.ToUpper();
        }
        private static void Error(string cmd, string msg)
        {
            cmd = $"[{cmd}]";
            Screen.WriteLine($"{cmd.To(Color.Gray)} {msg.To(Color.Red)}");
        }

        private static void Success(string cmd, string msg)
        {
            cmd = $"[{cmd}]";
            Screen.WriteLine($"{cmd.To(Color.Gray)} {msg.To(Color.GreenYellow)}");
        }

        private static string getMeasure(string type, string value)
        {
            if (type == "movej")
                return $"{value} rad/s";
            if (type == "movel")
                return $"{value} m/s";
            return $"{value} ?/s";
        }

        private static Thread consoleThread;
        private static void StartConsoleTitle()
        {
            consoleThread = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(200);
                    lock (MotherBoard.syncGuarder)
                    {
                        Console.Title =
                            $"UR5 CLI - state: {MotherBoard.CurrentState}, gstate: {MotherBoard.globalState}";
                    }
                }
            });
            consoleThread.Start();
        }
    }

    public static class Ex
    {
        public static string Round(this double val, int c) => Math.Round(val, c).ToString("F10");

    }
}
