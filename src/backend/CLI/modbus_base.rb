global Force_close = 255
global Position_close = $POS_CLOSE
global Speed_close = 255
out_reg2_close = Speed_close * 256 + Force_close
modbus_set_output_register("Speed_Force_Req", out_reg2_close, False)
modbus_set_output_register("Position_Req", Position_close, False)

modbus_set_output_signal("Go_to_Requested", True, False)
while(modbus_get_signal_status("Action_Status", False) == False):
    sync()
end

sleep(0.2)
while (not((modbus_get_signal_status("Obj_Det_Status1", False) == True) or (modbus_get_signal_status("Obj_Det_Status2", False) == True ))):
    sleep(0.1)
    sync()
end

sleep(0.2)
global obj_detect1 = modbus_get_signal_status("Obj_Det_Status1",False)
global obj_detect2 = modbus_get_signal_status("Obj_Det_Status2",False)
if ((obj_detect1 == False) and (obj_detect2 == True)):
    global Contact_opening = False
    global Contact_closing = True
else:
    if ((obj_detect1 == True) and (obj_detect2 == False)):
        global Contact_opening = True
        global Contact_closing = False
    else:
        global Contact_opening = False
        global Contact_closing = False
    end
end

modbus_set_output_signal("Go_to_Requested", False, False)
while(modbus_get_signal_status("Action_Status", False) == True):
    sync()
end
