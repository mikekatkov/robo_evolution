# CLI

base argumets:
```powershell
.\ur5-cli.exe -p PORT -i "IP"
```

print current position to terminal

```powershell
.\ur5-cli.exe -p PORT -i "IP" -e
```

print current position to file 'coord.pos'

```powershell
.\ur5-cli.exe -p PORT -i "IP" -ef
```

play file coordinates

```powershell
.\ur5-cli.exe -p PORT -i "IP" -z <file>
```

example file:
```
[-1.2565043465328065, -0.9156906312074228, -2.653504651191361, 0.6140473201146802, 1.1946722910583585, 0.18491323471501875]
[-2.1285621659490337, -0.8738926907411932, -2.667946025697509, 0.8644803257400226, 2.0817837231187304, 0.2689776828661254]
```

play coordinates

```powershell
.\ur5-cli.exe -p PORT -i "IP" -x "[-2.1, -0.87, -2.6, 0.86, 2.0, 0.2]"
```

move tools
допустимое значение: 255 - 0
```powershell
.\ur5-cli.exe -p PORT -i "IP" -w 255
```

override speed

```powershell
.\ur5-cli.exe -p PORT -i "IP" -s "1.200"
```

ovveride acceleration

```powershell
.\ur5-cli.exe -p PORT -i "IP" -q "1.200"
```
wait 5000ms on start program

```powershell
.\ur5-cli.exe -p PORT -i "IP" -ts 5000 
```

wait 5000ms on close program

```powershell
.\ur5-cli.exe -p PORT -i "IP" -tq 5000 
```

enable debug trace data output

```powershell
.\ur5-cli.exe -p PORT -i "IP" -debug
```

await masterboard state

```powershell
.\ur5-cli.exe -p PORT -i "IP" -d
```

enable freedrive

```powershell
.\ur5-cli.exe -p PORT -i "IP" -fd
```
