# UR-GUI Library

- JavaVersion: 6.0
- Platform: Linux Only
- Render: OpenGL
- Polyscope: 1.8.X
- URController: 1.8.X

- `URGUI-source.jar` - source code jar lib
- `URGUI.jar` - binary version lib


# Run on Windows

- install wsl
- install ubuntu from microsoft store
- run ubuntu
- run `sudo apt-get install libjava3d-java`
- run `sudo apt-get install libgluegen2-rt-java`
- run `sudo apt-get install openjdk-8-jre`
- run `java -jar .\URGUI.jar`
